Toyz
----


Technical considerations
========================

* `mmap`-ing a file is risky because of SIGBUS and/or portability issues.
* Reading and processing the file by buffer should improve memory demands.
* Data redundancy can be avoided regarding transaction and user IDs.
  We keep mappings of id -> struct and the structure also stores the ID. 
  We could use different structs for serialization and storage but that 
  might create confusion in so small a project.
  Still it can quickly become a problem.
* `cargo run -- in.csv > out.csv` -- don't spew garbage on stdout
* Each module should have its own errors.
* It's not specified if dispute/resolve/chargeback can be reverted but this could 
  lead into infinite problems and also they lack a proper ID.
* Parallelization isn't an option as transactions are read from a file and processed
  in chronological order.
* In production there should also be some kind of failsafe when it comes to
  signals or early exits. The client db should be backed-up from time to time.
* It's not clearly specified if the operation types are in a specific case so
  I'm going to follow lowercase example as generated from some other application 
  and drop the others as corrupted.


in.csv
======

type, client, tx, amount

| client:	 unique id u16
| type:	     deposit, withdraw, dispute, resolve, chargeback
| tx:		 transaction id u32
| amount:	 optional sum: f64 up to 4 places past decimal


out.csv
=======

client, held, total, locked

| client:		 check in.csv
| available:	 total - held
| held:		     f64, total - available. held for dispute
| total:		 f64, available + held
| locked:		 bool


Operations
==========

* Deposit/Withdraw

  type, client, tx, amount

  * Increase/decrease total. 
  * **On decrease don't allow a negative total/available!**

* Dispute

  type, client, tx

  * Search for a past op with tx ID and revert it using held and available.
  * **Total remains unchanged!**

* Resolve

  type, client, tx

  * Basically the dispute has been rejected.
  * Find a past op by tx and iff under dispute settle it.
  * Held amount is balanced with available. Total remains the same!


* Chargeback

  type, client, tx

  * The dispute has been accepted.
  * Final state of a dispute.
  * Client held and total funds are balanced by the amount under dispute. 
  * Client account is immediately frozen. 
  * Past transaction is deleted.


Example
=======

I *hope* I understood the operations correctly....

There are two scenarios when the user might spend money he doesn't have before settling a dispute.
Thus we should check on withdrawal that both total and available would remain positive.

**Remember**: `total = available + held` at any time
    
| deposit, 1, 1, 10 => total 10, available 10,
| dispute, 1, 1,    => total 10, available  0, held 10 (available -= tx.amount, held += tx.amount)
| resolve, 1, 1,    => total 10, available 10, held  0 (available += tx.amount, held -= tx.amount)
| charge,  1, 1     => total  0, available  0, held  0 (total     =- tx.amount, held =- tx.amount)
| 
| with     1, 2, 10 => total  0, available   0
| dispute  1, 2,    => total  0, available  10, held -10 (available += amount,      held -= tx.amount)
| resolve  1, 2,    => total  0, available   0, held   0 (available -= amount,      held += tx.amount)
| charge   1, 2,    => total 10, available  10, held   0 (available += tx.amount,   held += tx.amount)

There is also a sample dataset in `src/ops.csv` that should test *all* error cases.

