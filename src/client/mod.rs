pub mod error;

use super::transaction::{Transaction, Type};
use error::{Error, Result};
use rust_decimal::Decimal;
use std::collections::HashMap;

#[derive(Debug, Serialize)]
pub struct Client {
    #[serde(rename = "client")]
    id: u16,
    available: Decimal,
    held: Decimal,
    total: Decimal,
    locked: bool,
    #[serde(skip)]
    ops: HashMap<u32, Transaction>,
}

impl Default for Client {
    fn default() -> Self {
        Client {
            id: 0,
            available: dec!(0.0),
            held: dec!(0.0),
            total: dec!(0.0),
            locked: false,
            ops: HashMap::new(),
        }
    }
}

impl Client {
    pub fn new(id: u16) -> Client {
        Client {
            id,
            available: dec!(0.0),
            held: dec!(0.0),
            total: dec!(0.0),
            locked: false,
            ops: HashMap::new(),
        }
    }
    fn sanity(&self) -> Result<()> {
        if self.locked {
            return Err(Error::Blocked);
        }

        if self.total != self.held + self.available {
            return Err(Error::Integrity);
        }

        Ok(())
    }

    fn process_deposit(&mut self, t: Transaction) -> Result<()> {
        if t.get_type() != Type::Deposit {
            error!("invalid backend deposit operation!");
            return Err(Error::Integrity);
        }

        let tid = t.get_id();

        if self.ops.contains_key(&tid) {
            warn!("duplicate deposit operation!");
            return Err(Error::Duplicate);
        }

        let amt = t.get_amount().ok_or(Error::Integrity)?;

        self.available += amt;
        self.total += amt;
        self.ops
            .insert(tid, t)
            .map_or((), |val| error!("deposit duplicate key {:?}", val));

        debug!("deposit {} {:#?}", tid, self);

        Ok(())
    }

    fn process_withdraw(&mut self, t: Transaction) -> Result<()> {
        if t.get_type() != Type::Withdrawal {
            error!("invalid backend withdraw operation!");
            return Err(Error::Integrity);
        }

        let tid = t.get_id();

        if self.ops.contains_key(&tid) {
            warn!("duplicate withdraw operation!");
            return Err(Error::Duplicate);
        }

        let amt = t.get_amount().ok_or(Error::Integrity)?;

        if self.total - amt < dec!(0.0) || self.available - amt < dec!(0.0) {
            return Err(Error::InsufficientFunds);
        }

        self.available -= amt;
        self.total -= amt;

        self.ops
            .insert(tid, t)
            .map_or((), |val| error!("withdraw duplicate key {:?}", val));

        debug!("withdraw {} {:#?}", tid, self);

        Ok(())
    }

    fn process_dispute(&mut self, t: Transaction) -> Result<()> {
        if t.get_type() != Type::Dispute {
            error!("invalid backend withdraw operation!");
            return Err(Error::Integrity);
        }

        let tid = t.get_id();

        if !self.ops.contains_key(&tid) {
            return Ok(());
        }

        let op = self.ops.get_mut(&tid).ok_or(Error::Integrity)?;

        if op.get_dispute() {
            warn!("transaction {} already under dispute", op.get_id());
            return Ok(());
        }

        // this is where it gets tricky
        // available = total - held
        // held = total - available
        // total = available + held
        let am = op.get_amount().ok_or(Error::Integrity)?;
        match op.get_type() {
            Type::Deposit => {
                self.available -= am;
                self.held += am;
                op.set_dispute(true);
            }
            Type::Withdrawal => {
                self.available += am;
                self.held -= am;
                op.set_dispute(true);
            }
            _ => return Err(Error::Integrity),
        }

        debug!("dispute {} {:#?}", tid, self);

        Ok(())
    }

    fn process_resolve(&mut self, t: Transaction) -> Result<()> {
        if t.get_type() != Type::Resolve {
            error!("invalid backend resolve operation!");
            return Err(Error::Integrity);
        }

        let tid = t.get_id();

        if !self.ops.contains_key(&tid) {
            return Ok(());
        }

        let op = self.ops.get_mut(&tid).ok_or(Error::Integrity)?;

        if !op.get_dispute() {
            error!("transaction {} not under dispute", op.get_id());
            return Err(Error::NotApplicable);
        }

        let am = op.get_amount().ok_or(Error::Integrity)?;

        match op.get_type() {
            Type::Deposit => {
                self.available += am;
                self.held -= am;
            }
            Type::Withdrawal => {
                self.available -= am;
                self.held += am;
            }
            _ => return Err(Error::Integrity),
        }

        op.set_dispute(false);

        debug!("resolve {} {:#?}", tid, self);

        Ok(())
    }

    fn process_chargeback(&mut self, t: Transaction) -> Result<()> {
        if t.get_type() != Type::Chargeback {
            error!("invalid backend resolve operation!");
            return Err(Error::Integrity);
        }
        let tid = t.get_id();

        if !self.ops.contains_key(&tid) {
            return Ok(());
        }

        let op = self.ops.get_mut(&tid).ok_or(Error::Integrity)?;

        if !op.get_dispute() {
            error!("transaction {} not under dispute", op.get_id());
            return Err(Error::NotApplicable);
        }

        let am = op.get_amount().ok_or(Error::Integrity)?;

        match op.get_type() {
            Type::Deposit => {
                self.held -= am;
            }
            Type::Withdrawal => {
                self.held += am;
            }
            _ => return Err(Error::Integrity),
        }

        self.total = self.available + self.held;
        self.locked = true;
        self.ops.remove_entry(&tid);

        debug!("chargeback {} {:#?}", tid, self);

        Ok(())
    }

    pub fn process(&mut self, t: Transaction) -> Result<()> {
        self.sanity()?;

        t.sanity()?;

        match t.get_type() {
            Type::Deposit => self.process_deposit(t)?,
            Type::Withdrawal => self.process_withdraw(t)?,
            Type::Dispute => self.process_dispute(t)?,
            Type::Resolve => self.process_resolve(t)?,
            Type::Chargeback => self.process_chargeback(t)?,
        }

        Ok(())
    }
}
