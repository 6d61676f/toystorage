use crate::transaction::error as terr;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Blocked,
    Integrity,
    Duplicate,
    NotApplicable,
    InsufficientFunds,
    Transaction(terr::Error),
}

impl From<terr::Error> for Error {
    fn from(e: terr::Error) -> Self {
        Error::Transaction(e)
    }
}
