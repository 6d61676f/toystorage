pub mod error;

use super::{client::Client, transaction::Transaction};

use std::collections::HashMap;

pub struct Db {
    clients: HashMap<u16, Client>,
}

impl Db {
    pub fn new() -> Self {
        Db {
            clients: HashMap::new(),
        }
    }

    pub fn process(&mut self, t: Transaction) -> error::Result<()> {
        t.sanity()?;

        let cid = t.get_client();

        let c = self.clients.entry(cid).or_insert_with(|| Client::new(cid));

        c.process(t)?;

        Ok(())
    }

    pub fn serialize(&mut self) -> error::Result<()> {
        let vec: Vec<&Client> = self.clients.values().collect();

        let mut writer = csv::Writer::from_writer(std::io::stdout());

        for i in vec {
            writer.serialize(&i)?;
        }

        Ok(())
    }
}

impl Default for Db {
    fn default() -> Self {
        Db::new()
    }
}
