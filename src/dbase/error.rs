use crate::client::error as cerr;
use crate::transaction::error as terr;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Csv(csv::Error),
    Io(std::io::Error),
    Client(cerr::Error),
    Transaction(terr::Error),
    Misc(String),
}

impl From<csv::Error> for Error {
    fn from(e: csv::Error) -> Self {
        Error::Csv(e)
    }
}

impl From<cerr::Error> for Error {
    fn from(e: cerr::Error) -> Self {
        Error::Client(e)
    }
}

impl From<terr::Error> for Error {
    fn from(e: terr::Error) -> Self {
        Error::Transaction(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Io(e)
    }
}
