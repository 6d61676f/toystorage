use std::{fs::File, io::BufReader};

use log::{debug, error, warn};

use toyz::{
    dbase::{error, Db},
    transaction::Transaction,
};

fn main() -> error::Result<()> {
    env_logger::init();

    let arg = std::env::args()
        .nth(1)
        .ok_or_else(|| error::Error::Misc("Please pass input".to_string()))?;

    let mut reader = BufReader::new(File::open(&arg)?);

    let mut db = Db::new();

    csv::ReaderBuilder::new()
        .trim(csv::Trim::All)
        .comment(Some(b'#'))
        .flexible(true)
        .from_reader(&mut reader)
        .deserialize()
        .filter_map(|rec| rec.map_err(|err| warn!("parser error: {:?}", err)).ok())
        .map(|rec: Transaction| {
            debug!("{:?}", rec);
            db.process(rec)
        })
        .for_each(|rez| {
            if let Err(e) = rez {
                error!("{:?}", e);
            }
        });

    db.serialize()?;

    Ok(())
}
