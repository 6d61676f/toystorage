pub mod error;

use error::{Error, Result};

use rust_decimal::Decimal;
use serde::Deserialize;
use std::cmp::PartialEq;
use std::fmt::Debug;

#[derive(Debug, Deserialize, PartialEq, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum Type {
    Deposit,
    Withdrawal,
    Dispute,
    Resolve,
    Chargeback,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct Transaction {
    #[serde(alias = "type")]
    ty: Type,
    client: u16,
    #[serde(alias = "tx")]
    id: u32,
    amount: Option<Decimal>,
    #[serde(skip)]
    dispute: bool,
}

impl Transaction {
    pub fn sanity(&self) -> Result<()> {
        match self.ty {
            Type::Deposit | Type::Withdrawal => {
                if self.amount.is_none() {
                    Err(Error::MissingAmount)
                } else if self.amount.unwrap() <= dec!(0.0) {
                    Err(Error::InvalidAmount)
                } else {
                    Ok(())
                }
            }
            Type::Dispute | Type::Chargeback | Type::Resolve => {
                //even is amount is Some we don't care
                if self.amount.is_some() {
                    warn!("received dispute with amount");
                }
                Ok(())
            }
        }
    }

    pub fn get_amount(&self) -> Option<Decimal> {
        self.amount
    }
    pub fn get_type(&self) -> Type {
        self.ty
    }

    pub fn get_id(&self) -> u32 {
        self.id
    }

    pub fn get_dispute(&self) -> bool {
        self.dispute
    }

    pub fn set_dispute(&mut self, v: bool) {
        self.dispute = v;
    }

    pub fn get_client(&self) -> u16 {
        self.client
    }
}
